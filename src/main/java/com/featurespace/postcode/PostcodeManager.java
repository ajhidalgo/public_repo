/**
 * 
 */
package com.featurespace.postcode;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.Properties;

import com.featurespace.postcode.entity.json.JsonResponseGet;
import com.featurespace.postcode.entity.json.JsonResponseNearest;
import com.featurespace.postcode.entity.json.JsonResponseValidate;

/**
 * Runable class.
 * 
 * @author ajhidalgo
 *
 */
public class PostcodeManager {

	/* Constants. */
	private static final String CONFIG_PROPERTIES = "/config.properties";
	private static final String MESSAGES_PROPERTIES = "/messages.properties";

	/* Variables. */
	private static Properties prop;
	private static RestManager restManager;

	/**
	 * Main method.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			// Initialization.
			init();

			for (String arg : args) {

				// Title
				System.out.println(prop.getProperty(StringResources.MESSAGE_POSTCODE_TITLE) + arg);

				/*
				 * Validate the postcode parameter – invalid postcodes should
				 * produce an error message.
				 */
				JsonResponseValidate validatePostcode = validatePostcode(arg);

				if (validatePostcode.isResult()) {

					/*
					 * Print validation.
					 */
					System.out.println(prop.getProperty(StringResources.MESSAGE_POSTCODE_VALID));

					/*
					 * Print the country and region for that postcode.
					 */
					JsonResponseGet getPostcode = getPostcode(arg);
					if (getPostcode.getStatus() == 200) {
						System.out.println(prop.getProperty(StringResources.MESSAGE_POSTCODE_BELONGS) + getPostcode);
					} else {
						if (getPostcode.getError() != null) {
							System.err.println(getPostcode.getError());
						}
					}

					/*
					 * Print a list of the nearest postcodes, and their
					 * countries and regions.
					 */
					JsonResponseNearest nearestPostcodes = nearestPostcodes(arg);
					if (nearestPostcodes.getStatus() == 200) {
						System.out.println(prop.getProperty(StringResources.MESSAGE_POSTCODE_NEAREST));
						System.out.println(nearestPostcodes);
					} else {
						if (nearestPostcodes.getError() != null) {
							System.err.println(nearestPostcodes.getError());
						}
					}
				} else {
					if (validatePostcode.getStatus() != 200) {
						if (validatePostcode.getError() != null) {
							System.err.println(validatePostcode.getError());
						}
					} else {
						System.err.println(prop.getProperty(StringResources.MESSAGE_ERROR_INVALID));
						System.err.println();
					}
				}
			}
		} catch (IOException e) {
		}
	}

	/**
	 * Initialize the configuration.
	 * 
	 * @throws IOException
	 */
	private static void init() throws IOException {
		prop = new Properties();
		prop.load(PostcodeManager.class.getResourceAsStream(CONFIG_PROPERTIES));
		prop.load(PostcodeManager.class.getResourceAsStream(MESSAGES_PROPERTIES));
		restManager = new RestManager(prop);
	}

	/**
	 * Validates a postcode.
	 * 
	 * @param postcode
	 * @return
	 */
	private static JsonResponseValidate validatePostcode(String postcode) {
		JsonResponseValidate response = null;
		try {
			response = restManager.validatePostcode(postcode);
		} catch (MalformedURLException e) {
			response = new JsonResponseValidate(StringResources.MALFORMED_URL_EXCEPTION + e.getMessage());
		} catch (ProtocolException e) {
			response = new JsonResponseValidate(StringResources.PROTOCOLO_EXCEPTION + e.getMessage());
		} catch (IOException e) {
			response = new JsonResponseValidate(StringResources.IO_EXCEPTION + e.getMessage());
		}
		return response;
	}

	/**
	 * Gets data from a postcode.
	 * 
	 * @param postcode
	 * @return
	 */
	private static JsonResponseGet getPostcode(String postcode) {
		JsonResponseGet response = null;
		try {
			response = restManager.getPostcode(postcode);
		} catch (MalformedURLException e) {
			response = new JsonResponseGet(StringResources.MALFORMED_URL_EXCEPTION + e.getMessage());
		} catch (ProtocolException e) {
			response = new JsonResponseGet(StringResources.PROTOCOLO_EXCEPTION + e.getMessage());
		} catch (IOException e) {
			response = new JsonResponseGet(StringResources.IO_EXCEPTION + e.getMessage());
		}
		return response;
	}

	/**
	 * Gets nearest postcodes from a postcode.
	 * 
	 * @param postcode
	 * @return
	 */
	private static JsonResponseNearest nearestPostcodes(String postcode) {
		JsonResponseNearest response = null;
		try {
			response = restManager.nearestPostcodes(postcode);
		} catch (MalformedURLException e) {
			response = new JsonResponseNearest(StringResources.MALFORMED_URL_EXCEPTION + e.getMessage());
		} catch (ProtocolException e) {
			response = new JsonResponseNearest(StringResources.PROTOCOLO_EXCEPTION + e.getMessage());
		} catch (IOException e) {
			response = new JsonResponseNearest(StringResources.IO_EXCEPTION + e.getMessage());
		}
		return response;
	}

}
