/**
 * 
 */
package com.featurespace.postcode.entity;

/**
 * Postcode bean.
 * 
 * @author ajhidalgo
 *
 */
public class Postcode {

	/** Postcode. */
	private String postcode;

	/** Country. */
	private String country;

	/** Region. */
	private String region;

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public String toString() {
		return "[postcode=" + postcode + ", country=" + country + ", region=" + region + "]";
	}

}
