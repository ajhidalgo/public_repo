/**
 * 
 */
package com.featurespace.postcode.entity.json;

import java.util.List;

import com.featurespace.postcode.entity.Postcode;

/**
 * JSON response for nearest postcodes.
 * 
 * @author ajhidalgo
 *
 */
public class JsonResponseNearest extends JsonResponse {

	/** Result of the request. */
	private List<Postcode> result;

	/**
	 * Default constructor.
	 */
	public JsonResponseNearest() {
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param error
	 */
	public JsonResponseNearest(String error) {
		super(error);
	}

	public List<Postcode> getResult() {
		return result;
	}

	public void setResult(List<Postcode> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Postcode pc : result) {
			sb.append(pc.getPostcode()).append(": ").append(pc.getRegion()).append(", ").append(pc.getCountry()).append("\n");
		}
		return sb.toString();
	}

}
