/**
 * 
 */
package com.featurespace.postcode.entity.json;

/**
 * Abstract class for a JSON response.
 * 
 * @author ajhidalgo
 *
 */
public abstract class JsonResponse {

	/** Status. */
	private int status;

	/** Error message. */
	private String error;

	/**
	 * Default constructor.
	 */
	public JsonResponse() {
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param error
	 */
	public JsonResponse(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
