/**
 * 
 */
package com.featurespace.postcode.entity.json;

/**
 * JSON response for postcode validation.
 * 
 * @author ajhidalgo
 *
 */
public class JsonResponseValidate extends JsonResponse {

	/** Result of the request. */
	private boolean result;

	/**
	 * Default constuctor.
	 */
	public JsonResponseValidate() {
	}

	/**
	 * Constructor with error message.
	 * @param error
	 */
	public JsonResponseValidate(String error) {
		super(error);
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}
