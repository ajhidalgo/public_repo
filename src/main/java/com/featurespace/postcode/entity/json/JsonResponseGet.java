/**
 * 
 */
package com.featurespace.postcode.entity.json;

import com.featurespace.postcode.entity.Postcode;

/**
 * JSON response for postcode info.
 * 
 * @author ajhidalgo
 *
 */
public class JsonResponseGet extends JsonResponse {

	/** Result of request. */
	private Postcode result;

	/**
	 * Default constructor.
	 */
	public JsonResponseGet() {
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param error
	 */
	public JsonResponseGet(String error) {
		super(error);
	}

	public Postcode getResult() {
		return result;
	}

	public void setResult(Postcode result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return result.getRegion() + ", " + result.getCountry();
	}

}
