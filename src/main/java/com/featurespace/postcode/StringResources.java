/**
 * 
 */
package com.featurespace.postcode;

/**
 * String resources.
 * 
 * @author ajhidalgo
 *
 */
public class StringResources {
	
	public static final String MESSAGE_POSTCODE_TITLE = "message.postcode.title";
	public static final String MESSAGE_POSTCODE_BELONGS = "message.postcode.belongs";
	public static final String MESSAGE_POSTCODE_NEAREST = "message.postcode.nearest";
	public static final String MESSAGE_POSTCODE_VALID = "message.postcode.valid";
	
	public static final String MESSAGE_ERROR_INVALID = "message.error.invalid";
	public static final String MESSAGE_ERROR_REST_CALL = "message.error.rest.call";
	public static final String MESSAGE_ERROR_REST_CALL_TIMEOUT = "message.error.rest.call.timeout";
	
	public static final String MALFORMED_URL_EXCEPTION = "MalformedURLException: ";
	public static final String PROTOCOLO_EXCEPTION = "ProtocoloException: ";
	public static final String IO_EXCEPTION = "IOException: ";
	
	public static final String URL_ROOT = "URL.root";
	public static final String URL_VALIDATE_APPEND = "URL.validate.append";
	public static final String URL_NEAREST_APPEND = "URL.nearest.append";

	public static final String METHOD_GET = "GET";
	public static final String PROPERTY_ACCEPT = "Accept";
	public static final String PROPERTY_APPLICATION_JSON = "application/json";
	
	public static final String WHITESPACE = " ";

}
