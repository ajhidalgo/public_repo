/**
 * 
 */
package com.featurespace.postcode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Properties;

import com.featurespace.postcode.entity.json.JsonResponse;
import com.featurespace.postcode.entity.json.JsonResponseGet;
import com.featurespace.postcode.entity.json.JsonResponseNearest;
import com.featurespace.postcode.entity.json.JsonResponseValidate;
import com.google.gson.Gson;

/**
 * Rest call manager.
 * 
 * @author ajhidalgo
 *
 */
public class RestManager {

	/* Variables. */
	private Properties prop;
	private String URLroot;

	/**
	 * Constructor.
	 * 
	 * @param prop
	 */
	public RestManager(Properties prop) {
		this.prop = prop;
		URLroot = prop.getProperty(StringResources.URL_ROOT);
	}

	/**
	 * Validate a Postcode.
	 * 
	 * @param postcode
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	public JsonResponseValidate validatePostcode(String postcode)
			throws MalformedURLException, IOException, ProtocolException {
		// Preparing URL.
		StringBuilder sbValidate = new StringBuilder(URLroot);
		sbValidate.append(postcode).append(prop.getProperty(StringResources.URL_VALIDATE_APPEND));
		JsonResponseValidate response = new JsonResponseValidate();
		return (JsonResponseValidate) callGet(sbValidate.toString(), response);
	}

	/**
	 * Gets info of a Postcode.
	 * 
	 * @param postcode
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	public JsonResponseGet getPostcode(String postcode) throws MalformedURLException, IOException, ProtocolException {
		// Preparing URL.
		StringBuilder sbGet = new StringBuilder(URLroot);
		sbGet.append(postcode);
		JsonResponseGet response = new JsonResponseGet();
		return (JsonResponseGet) callGet(sbGet.toString(), response);
	}

	/**
	 * Gets nearest postcodes from a Postcode.
	 * 
	 * @param postcode
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	public JsonResponseNearest nearestPostcodes(String postcode)
			throws MalformedURLException, IOException, ProtocolException {
		// Preparing URL.
		StringBuilder sbNearest = new StringBuilder(URLroot);
		sbNearest.append(postcode).append(prop.getProperty(StringResources.URL_NEAREST_APPEND));
		JsonResponseNearest response = new JsonResponseNearest();
		return (JsonResponseNearest) callGet(sbNearest.toString(), response);
	}

	/**
	 * Makes the rest call.
	 * 
	 * @param urlString
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	private JsonResponse callGet(String urlString, JsonResponse response)
			throws MalformedURLException, IOException, ProtocolException {
		URL url = new URL(urlString);
		
		try {
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(StringResources.METHOD_GET);
			conn.setRequestProperty(StringResources.PROPERTY_ACCEPT, StringResources.PROPERTY_APPLICATION_JSON);
			conn.setConnectTimeout(5000);
	
			if (conn.getResponseCode() != 200) {
				// Error data message.
				System.err.println(prop.getProperty(StringResources.MESSAGE_ERROR_REST_CALL) + conn.getResponseCode()
						+ StringResources.WHITESPACE + conn.getResponseMessage());
			}

			// Processing response.
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			Gson gson = new Gson();
			response = gson.fromJson(br, response.getClass());
	
			conn.disconnect();
		} catch (SocketTimeoutException e) {
			// Error data message.
			System.err.println(prop.getProperty(StringResources.MESSAGE_ERROR_REST_CALL_TIMEOUT));
			throw new IOException(e);
		}

		return response;
	}

}
