/**
 * 
 */
package com.featurespace.postcode;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 * @author ajhidalgo
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PostcodeManagerTest {

	@Test
	public void testA_Correct() {
		String[] args = new String[] {"CB3 0FA"};
		PostcodeManager.main(args);
	}

	@Test
	public void testB_NoExists() {
		String[] args = new String[] {"CB3 0F3"};
		PostcodeManager.main(args);
	}

	@Test
	public void testC_BadPostcode() {
		String[] args = new String[] {"123456"};
		PostcodeManager.main(args);
	}

}
